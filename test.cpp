#include "src/core.h"
#include <iostream>

int main(){
	std::cout<<"\n===========================\n";
	hls::stream<ap_axis_str> input_stream;
	hls::stream<ap_axis_str> output_stream;

	ap_axis_str strm_val_in;
	ap_axis_str strm_val_out;
	for (int j = 0; j<66; j++){
		std::cout<<"\n\n";

		for (int i = 0; i<NSAMPLES; i++){
			strm_val_in.data = i;
			strm_val_in.keep = 3;
			strm_val_in.user = 0;
			strm_val_in.last = (i == NSAMPLES-1) ? 1:0;
			input_stream.write(strm_val_in);
		}

		fir(input_stream, output_stream);


		while(!output_stream.empty()) {
			output_stream.read(strm_val_out);
			std::cout<<strm_val_out.data;

			if (strm_val_out.last) {
				std::cout<<"\n";
			}else if (strm_val_out.user){
				std::cout<<" ; ";
			} else {
				std::cout<<", ";
			}
		}
	}
	std::cout<<"===========================\n\n";
	return(0);
}
