#include "core.h"
#ifndef __SYNTHESIS__
#include <iostream>
#endif

ap_int<16> bin_SSR[NCHAN][NBINS] = {0};
ap_uint<6> channel_counter = 0;
ap_int<16> bins[NBINS] = {0};

ap_axis_str word_to_stream(word false_stream, int last = 0){
#pragma HLS inline
	ap_axis_str converted;
	converted.data = false_stream;
	converted.keep = 3;
	converted.user = last;
	converted.last = last;
	return(converted);
}


void fir(hls::stream<ap_axis_str> &input_stream, hls::stream<ap_axis_str> &output_stream){
#pragma HLS INTERFACE axis port=input_stream
#pragma HLS INTERFACE axis port=output_stream
#pragma HLS INTERFACE ap_ctrl_none port=return
	ap_uint<10> coefficients[NBINS] = {0,0,0,0,0,0,0,0,2,4,6,7,9,11,12,13,13,12,11,9,7,6,4,2,0,0,0,0,0,0,0,0};
	ap_uint<6> channel = channel_counter;
	#ifndef __SYNTHESIS__
	std::cout<<"channel: "<<channel<<"\n";
	#endif
	#pragma HLS array_partition variable=bins

	for (int i=0; i<NSAMPLES; i++){
		bool last = (i==(NSAMPLES-1))? 1:0;
		ap_int<16> weighted_sum = 0;
		for (int j=NBINS-1; j>=0; j--){
			bins[j] = (j==0)? input_stream.read().data:bins[j-1];
			weighted_sum += bins[j]*coefficients[j];
		}
		output_stream.write(word_to_stream(weighted_sum, last));


	}
	channel_counter++;
	if (channel_counter == NCHAN){
		channel_counter = 0;
	}
	for (int j=0; j<NBINS; j++){
		bin_SSR[channel][j] = bins[j];
		bins[j] = bin_SSR[channel_counter][j];
	}


}
